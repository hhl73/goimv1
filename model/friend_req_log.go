package model

import (
	"im-v1/database"
	"time"
)

// 好友请求日志
type FriendReqLog struct {
	Id       int       `json:"id" gorm:"column:id"`               // 自增主键
	MailFrom string    `json:"mail_from" gorm:"column:mail_from"` // 好友请求发起方
	MailTo   string    `json:"mail_to" gorm:"column:mail_to"`     // 好友请求接收方
	SendTime time.Time `json:"send_time" gorm:"column:send_time"` // 好友请求发起时间
	ReqBody  string    `json:"req_body" gorm:"column:req_body"`   // 好友请求消息内容
}

func (e *FriendReqLog) TableName() string {
	return "friend_req_log"
}

// 转换为 FriendReqOds
func (e *FriendReqLog) TransformOds() *FriendReqOds {
	return &FriendReqOds{e.MailFrom, e.SendTime, e.ReqBody}
}

// 新建 FriendReqLog 结构体
func NewFriendReqLog(from, to, body string) *FriendReqLog {
	return &FriendReqLog{0, from, to, time.Now(), body}
}

// 插入新的记录
func InsertFriendReqLog(e *FriendReqLog) {
	database.Db.Create(e)
}
