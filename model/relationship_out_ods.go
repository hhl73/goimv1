package model

import "im-v1/database"

// 对外展示用户关系
type RelationshipOds struct {
	MailTo        string `json:"mail_to" gorm:"column:mail_to"`               // 好友
	RelationGrade int    `json:"relation_grade" gorm:"column:relation_grade"` // 关系等级
	Secret        string `json:"secret" gorm:"column:secret"`                 // 联系密钥-唯一
}

func (e *RelationshipOds) TableName() string {
	return "relationship"
}

// 获取好友信息
func FindGoodFriendshipOds(from string) []RelationshipOds {
	ods := make([]RelationshipOds, 0)
	database.Db.Where("mail_from = ?", from).Find(&ods)
	return ods
}
