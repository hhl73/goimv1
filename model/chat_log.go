package model

import (
	"im-v1/database"
	"time"
)

// 聊天消息日志
type ChatLog struct {
	Id       int       `json:"id" gorm:"column:id"`               // 自增主键
	MailFrom string    `json:"mail_from" gorm:"column:mail_from"` // 消息发送人
	MailTo   string    `json:"mail_to" gorm:"column:mail_to"`     // 消息接收人
	SendTime time.Time `json:"send_time" gorm:"column:send_time"` // 发送时间
	ChatBody string    `json:"chat_body" gorm:"column:chat_body"` // 消息体
}

func (e *ChatLog) TableName() string {
	return "chat_log"
}

func NewChatLog(from, to, body string) *ChatLog {
	return &ChatLog{0, from, to, time.Now(), body}
}

// 查润新记录
func (e *ChatLog) Insert() {
	database.Db.Create(e)
}
