package model

import "im-v1/database"

// 对外展示的用户信息
type UsrOds struct {                                             // 自增主键
	Mailbox  string `json:"mailbox" gorm:"column:mailbox" form:"mailbox" json:"mailbox" binding:"required"`     // 邮箱-唯一
	Nickname string `json:"nickname" gorm:"column:nickname" form:"nickname" json:"nickname" binding:"required"` // 昵称
	HeadUrl  string `json:"head_url" gorm:"column:head_url"`                                                    // 头像url
	Status   int    `json:"status" gorm:"column:status"`                                                        // 个人状态
}

func (e *UsrOds) TableName() string {
	return "usr"
}

// 根据 nickname 查找
func FindByNickname(nickname string) []UsrOds {
	usrs := make([]UsrOds, 0)
	database.Db.Where("nickname = ?", nickname).Find(&usrs)
	return usrs
}