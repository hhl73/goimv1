// 好友请求显示
package model

import (
	"im-v1/database"
	"time"
)

type FriendReqOds struct {
	MailFrom string    `json:"mail_from" gorm:"column:mail_from" form:"mail_from" binding:"required"` // 好友请求发起方
	SendTime time.Time `json:"send_time" gorm:"column:send_time" form:"send_time" binding:"required"` // 好友请求发起时间
	ReqBody  string    `json:"req_body" gorm:"column:req_body"`                                       // 好友请求消息内容
}

func (e *FriendReqOds) TableName() string {
	return "friend_req_log"
}

// 查询记录是否存在 -> 存在 true
func (e *FriendReqOds) ExistFriendReq(to string) bool {
	raw := database.Db.Where("mail_from = ? and mail_to = ? and send_time = ?", e.MailFrom, to, e.SendTime.Format("2006-01-02 15:04:05")).Find(&FriendReqLog{})
	return raw.RowsAffected != 0
}
