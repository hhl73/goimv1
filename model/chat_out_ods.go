package model

import "time"

// 聊天消息-对外
type ChatOds struct {
	MailFrom string    `json:"mail_from" gorm:"column:mail_from"` // 消息发送人
	SendTime time.Time `json:"send_time" gorm:"column:send_time"` // 发送时间
	ChatBody string    `json:"chat_body" gorm:"column:chat_body"` // 消息体
}

func (e *ChatOds) TableName() string {
	return "chat_log"
}

// 转换为 ChatOds
func (e *ChatLog) TransformOds() *ChatOds {
	return &ChatOds{e.MailFrom, e.SendTime, e.ChatBody}
}
