package routers

import (
	"github.com/gin-gonic/gin"
)

type Register func(*gin.Engine)

// 同时支持预先设置router
var RegisteredRouter []Register

// =================================================================================
// 建议在”main“使用此方法，可以选择两种方式：
// 1. var registeredRouter = []Register{xxx.Router}
//    main文件只要调用routers.Init()即可，无需传参
// 2. 直接在main传入需要注册router：routers.Init(xx.Router, xxx.Router)
// 对外开放方法，实现批量注册路由的功能
func Init(routers ...Register) *gin.Engine {
	// 注册路由
	rs := append([]Register{}, routers...)
	rs = append(rs, RegisteredRouter...)

	r := gin.Default()
	// 遍历调用方法
	for _, register := range rs {
		register(r)
	}
	return r
}
