// 注册 & 登录 模块 - 路由
package regist

import "github.com/gin-gonic/gin"

func Router(e *gin.Engine) {
	// 分区路径
	r := e.Group("/register")

	// 核心
	newUsr(r)              // 注册新用户
	getRegistrationCode(r) // 获取邮箱验证码
	loginUsr(r)            // 用户登录
}
