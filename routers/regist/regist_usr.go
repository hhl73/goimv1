package regist

import (
	"github.com/gin-gonic/gin"
	"im-v1/model"
	"im-v1/routers"
	"im-v1/service"
	"net/http"
)

// 注册新账号
// 入参：mailbox、code、password、nickname
// 出参：token，model.UsrOds
func newUsr(r *gin.RouterGroup) {
	r.POST("/newUsr", func(c *gin.Context) {

		// 参数校验 & 绑定到结构体
		code := c.PostForm("code")
		var usr model.Usr
		if err := c.Bind(&usr); code == "" || err != nil {
			// 参数不合法
			routers.ReturnRequestInvalid(c)
			return
		}

		// 业务核心
		token, err := service.Register(&usr, code)

		// 错误路径
		if err != nil {
			routers.ReturnRequestInvalid(c, err.Error())
			return
		}

		// 正确路径
		c.JSON(http.StatusOK, gin.H{
			"usr":   usr.TransformOds(),
			"token": token,
		})
	})
}

// 获取邮箱注册验证码
// 入参：mailbox
func getRegistrationCode(r *gin.RouterGroup) {
	r.POST("/getRegistrationCode", func(c *gin.Context) {

		// 参数校验
		mailbox := c.PostForm("mailbox")
		if mailbox == "" {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心业务
		msg := service.GetAuthCode(mailbox)
		statusCode := http.StatusOK // 默认状态码

		// 错误路径
		if msg != "" {
			statusCode = http.StatusNotAcceptable
		}

		// 正确路径
		c.JSON(statusCode, gin.H{"msg": msg})
	})
}

// 登录账号
// 入参：mailbox、password
// 出参：token、model.UsrOds
func loginUsr(r *gin.RouterGroup) {
	r.POST("/loginUsr", func(c *gin.Context) {

		// 参数校验
		mailbox, password := c.PostForm("mailbox"), c.PostForm("password")
		if mailbox == "" || password == "" {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 业务核心
		token, usr, err := service.Login(mailbox, password)

		// 错误路径
		if err != nil {
			routers.ReturnRequestInvalid(c, err.Error())
			return
		}

		// 正确路径
		c.JSON(http.StatusOK, gin.H{
			"usr":   usr,
			"token": token,
		})
	})
}
