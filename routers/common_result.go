// 常用返回模式
package routers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// 返回：400 - http.StatusBadRequest
// "msg": "request invalid"
// 如果还有多于参数会拼接在 msg 字段后面
func ReturnRequestInvalid(c *gin.Context, errs ...string) {
	msg := "request invalid"

	// 有后续则增加冒号
	if len(errs) > 0 {
		msg = msg + ": "
	}
	// 如果是大量拼接那么就需要使用到 strings.Builder{}
	if len(errs) > 10 {
		builder := strings.Builder{}
		builder.WriteString(msg)
		for _, e := range errs {
			builder.WriteString(e)
			builder.WriteString(" - ")
		}
		msg = builder.String()
	} else {
		// 拼接错误信息
		for _, e := range errs {
			msg = msg + e + " - "
		}
	}

	// 设置返回
	c.JSON(http.StatusBadRequest, gin.H{
		"msg": msg,
	})
}
