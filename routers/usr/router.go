// 用户管理模块 - 路由
package usr

import (
	"github.com/gin-gonic/gin"
	"im-v1/middleware"
)

func Router(e *gin.Engine) {
	// 给表单限制上传大小 (默认 32 MiB)
	e.MaxMultipartMemory = 4 << 20 // 4 MiB

	// 分区路径
	r := e.Group("/usr")

	// 中间件
	r.Use(middleware.Authentic())

	// 核心
	setHeadImg(r)    // 设置头像
	getUsrDetails(r) // 获取用户详情
}
