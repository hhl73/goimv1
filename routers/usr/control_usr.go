package usr

import (
	"github.com/gin-gonic/gin"
	"im-v1/config"
	"im-v1/routers"
	"im-v1/service"
	"net/http"
)

// 设置个人头像
// 入参：token、file
// 出参：headURL
func setHeadImg(r *gin.RouterGroup) {
	r.POST("/setHeadImg", func(c *gin.Context) {

		// 参数校验 & token身份判断
		mailbox := c.PostForm("mailbox")
		tokenMailbox, exists := c.Get(config.TokenMail)
		if mailbox == "" || !exists || mailbox != tokenMailbox {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 文件校验判断
		img, err := c.FormFile("newHeadImg")
		if err != nil || img == nil {
			routers.ReturnRequestInvalid(c, "no file was received")
			return
		}

		// 核心处理
		headUrl, err := service.SetHeadImg(mailbox, img)

		// 错误路径
		if err != nil {
			routers.ReturnRequestInvalid(c)
		}

		// 正确路径
		c.JSON(http.StatusOK, gin.H{
			"headURL": headUrl,
		})
	})
}

// 获取用户详情信息
// 入参：mailbox
// 出参：model.UsrOds
func getUsrDetails(r *gin.RouterGroup) {
	r.POST("/getUsrDetails", func(c *gin.Context) {

		// 参数校验
		mailbox := c.PostForm("mailbox")
		if mailbox == "" {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心处理
		usr, _ := service.FindUsrByMailbox(mailbox)

		// 返回结果
		c.JSON(http.StatusOK, gin.H{
			"usr": usr,
		})
	})
}
