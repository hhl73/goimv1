// 好友关系模块
package relationship

import (
	"github.com/gin-gonic/gin"
	"im-v1/middleware"
)

func Router(e *gin.Engine) {
	// 分区路径
	r := e.Group("/relationship")

	// 中间件
	r.Use(middleware.Authentic())

	// 核心
	findUsrByMailBox(r)   // 邮箱查找
	findUsrByNickname(r)  // 昵称查找
	addRelation(r)        // 添加好友
	getRelationRequest(r) // 获取好友请求
	acceptRelation(r)     // 同意好友请求
	delRelation(r)        // 删除好友
	showRelation(r)       // 获取好友关系
}
