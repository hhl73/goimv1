package relationship

import (
	"github.com/gin-gonic/gin"
	"im-v1/config"
	"im-v1/model"
	"im-v1/routers"
	"im-v1/service"
	"log"
	"net/http"
)

// 用户查询 - 通过账户邮箱
// 入参：mailbox
// 出参: model.UsrOds、exists
func findUsrByMailBox(r *gin.RouterGroup) {
	r.POST("/findUsrByMailBox", func(c *gin.Context) {
		// 参数校验
		mailbox := c.PostForm("mailbox")
		if mailbox == "" {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心
		usr, exists := service.FindUsrByMailbox(mailbox)

		// 返回结果
		c.JSON(http.StatusOK, gin.H{
			"usr":    usr,
			"exists": exists,
		})
	})
}

// 用户查询 - 通过账户昵称
// 入参：nickname
// 出参: model.UsrOds、exists
func findUsrByNickname(r *gin.RouterGroup) {
	r.POST("/findUsrByNickname", func(c *gin.Context) {
		// 参数校验
		nickname := c.PostForm("nickname")
		if nickname == "" {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心
		usrs, exists := service.FindUsrByNickname(nickname)

		// 返回结果
		c.JSON(http.StatusOK, gin.H{
			"usr":    usrs,
			"exists": exists,
		})
	})
}

// 建立好友关系
// 入参：targetMailbox(添加目标)、reqBody
// 出参：msg
func addRelation(r *gin.RouterGroup) {
	r.POST("/addRelationShip", func(c *gin.Context) {
		// 参数校验
		targetMailbox, reqBody := c.PostForm("targetMailbox"), c.PostForm("reqBody")
		fromMailbox, exists := c.Get(config.TokenMail)
		if targetMailbox == "" || reqBody == "" || !exists {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心
		from, ok := fromMailbox.(string)
		if !ok {
			log.Println("[relation_usr AddRelation] fromMailbox.(string) -> Failed")
			routers.ReturnRequestInvalid(c)
		}
		msg := service.AddRelation(from, targetMailbox, reqBody)

		// 返回结果
		c.JSON(http.StatusOK, gin.H{
			"msg": msg,
		})
	})
}

// 好友请求显示
// 入参：仅需token
// 出参: model.FriendReqOds
func getRelationRequest(r *gin.RouterGroup) {
	r.POST("/getRelationRequest", func(c *gin.Context) {
		// 参数校验
		mailbox, exists := c.Get(config.TokenMail)
		if !exists {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心
		reqs := service.GetRelationRequest(mailbox.(string))

		// 返回结果
		c.JSON(http.StatusOK, gin.H{
			"reqs": reqs,
		})
	})
}

// 同意好友申请
// 入参：mail_from、send_time (配合 to(token中获取，即用户自己) -> 组成唯一id，防止非法添加)
// 出参：msg
func acceptRelation(r *gin.RouterGroup) {
	r.POST("/acceptRelation", func(c *gin.Context) {
		// 参数校验
		var ods model.FriendReqOds
		to, exists := c.Get(config.TokenMail)
		if err := c.Bind(&ods); err != nil || !exists {
			// 参数不合法
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心
		msg := service.AcceptRelation(ods, to.(string))

		// 返回结果
		c.JSON(http.StatusOK, gin.H{
			"msg": msg,
		})
	})
}

// 删除好友 -> 单方面删除的
// 入参：targetMailbox(删除目标)
// 出参：msg
func delRelation(r *gin.RouterGroup) {
	r.POST("/delRelation", func(c *gin.Context) {
		// 参数校验
		targetMailbox := c.PostForm("targetMailbox")
		fromMailbox, exists := c.Get(config.TokenMail)
		if targetMailbox == "" || !exists {
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心
		msg := service.DelRelation(fromMailbox.(string), targetMailbox)

		// 返回结果
		c.JSON(http.StatusOK, gin.H{
			"msg": msg,
		})
	})
}

// 好友关系显示
// 出参: model.RelationshipOds
func showRelation(r *gin.RouterGroup) {
	r.POST("/showRelation", func(c *gin.Context) {
		// 参数校验
		mailbox, exists := c.Get(config.TokenMail)
		if !exists {
			// 参数不合法
			routers.ReturnRequestInvalid(c)
			return
		}

		// 核心
		ods := service.ShowFriendShip(mailbox.(string))

		// 返回结果
		c.JSON(http.StatusOK, gin.H{
			"friends": ods,
		})
	})
}
