// 私聊 - 一对一聊天
package chat

import (
	"github.com/gin-gonic/gin"
	"im-v1/config"
	"im-v1/routers"
	"im-v1/service"
	"net/http"
)

// 私聊发送信息
// 入参：mailTo、chatBody
// 出参：200 -> 发送成功， 400 -> 发送失败：非法操作， 410 -> 好友已经删除
func sendMsg(r *gin.RouterGroup) {
	r.POST("/sendMsg", func(c *gin.Context) {

		// 参数校验
		mailTo, chatBody := c.PostForm("mailTo"), c.PostForm("chatBody")
		mailFrom, exists := c.Get(config.TokenMail)
		if mailTo == "" || chatBody == "" || !exists {
			// 参数不合法
			routers.ReturnRequestInvalid(c)
			return
		}

		// 业务核心
		code, msg := service.SendPrivateMsg(mailFrom.(string), mailTo, chatBody)

		// 返回路径
		c.JSON(code, gin.H{
			"msg": msg,
		})
	})
}

// 接受个人消息
// 出参：model.ChatOds
func getMsg(r *gin.RouterGroup) {
	r.POST("/getMsg", func(c *gin.Context) {

		// 参数校验
		mailbox, exists := c.Get(config.TokenMail)
		if !exists {
			// 参数不合法
			routers.ReturnRequestInvalid(c)
			return
		}

		// 业务核心
		chats := service.GetMsg(mailbox.(string))

		// 返回路径
		c.JSON(http.StatusOK, gin.H{
			"chats": chats,
		})
	})
}
