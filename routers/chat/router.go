// 聊天模块
package chat

import (
	"github.com/gin-gonic/gin"
	"im-v1/middleware"
)

func Router(e *gin.Engine) {
	// 分区路径
	r := e.Group("/chat")

	// 中间件
	r.Use(middleware.Authentic())

	// 核心
	sendMsg(r) // 发送信息
	getMsg(r)  // 接受信息
}
