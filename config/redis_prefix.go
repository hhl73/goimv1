// 存储各种key的信息
package config

// redis - prefix
var (
	RegisterCode  = "register_"         // 邮箱注册码
	RelationMail  = "relation_mailbox_" // 关系申请接收方mailbox
	ChatMail      = "chat_mailbox_"     // 聊天信息接收方mailbox
	ChatConnector = "<->"               // chatKey连接符
)

// token信息
var (
	TokenId   = "token_id"
	TokenMail = "token_mailbox"
	TokenNick = "token_nickname"
)
