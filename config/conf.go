package config

// 基本配置文件信息
type Conf struct {
	Dbase      Database    `yaml:"Database"`
	Redis      RedisConfig `yaml:"RedisConfig"`
	MailConfig SenderMsg   `yaml:"SenderMsg"`
	Other      Other       `yaml:"Other"`
}

// mysql配置
type Database struct {
	Host     string `yaml:"Host"`
	Port     int    `yaml:"Port"`
	User     string `yaml:"User"`
	Password string `yaml:"Password"`
	DBName   string `yaml:"DBName"`
}

// redis配置
type RedisConfig struct {
	Addr     string `yaml:"Addr"`     //"127.0.0.1:6379",
	Password string `yaml:"Password"` // no password set
	DB       int    `yaml:"DB"`       // use default DB
}

// 发件人配置
type SenderMsg struct {
	Sender string `yaml:"Sender"` // 发送人
	Alias  string `yaml:"Alias"`  // 发送人别名
	Pass   string `yaml:"Pass"`   // 授权码
	Host   string `yaml:"Host"`   // 所属服务器
	Port   int    `yaml:"Port"`   // 发送端口
}

// 其他配置:
// * headUrl 图片存储地址
// * jwtSecret 密钥
type Other struct {
	HeadStoreUrl string `yaml:"HeadStoreUrl"`
	JwtSecret    string `yaml:"JwtSecret"`
}
