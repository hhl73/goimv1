package config

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

// 读取基本配置信息
var Tconf Conf

// 自动调用初始化
func init() {
	// 配置文件，优先调用！
	err := InitFile("./conf.yaml")
	if err != nil {
		log.Fatal("[启动错误 - 配置文件]：", err)
		return
	}
	fmt.Println(Tconf)
}

func InitFile(path string) error {
	ReConf, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
		return err
	}
	return yaml.Unmarshal(ReConf, &Tconf)
}
