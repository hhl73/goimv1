package main

import (
	"im-v1/database"
	"im-v1/routers"
	"im-v1/routers/chat"
	"im-v1/routers/regist"
	"im-v1/routers/relationship"
	"im-v1/routers/usr"
)

// 配置信息已经解耦到conf.yaml，只需要修改该文件不需要改代码

// 程序入口
func main() {

	// 数据库 & redis 初始化
	database.InitMysql()
	database.InitRedis()
	defer func() {
		database.CloseMysql()
		database.CloseRedis()
	}()

	// 注册路由
	routers.RegisteredRouter = []routers.Register{
		regist.Router,
		usr.Router,
		relationship.Router,
		chat.Router,
	}

	r := routers.Init()
	r.Run(":8080")
}
