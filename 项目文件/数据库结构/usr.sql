/*
Navicat MySQL Data Transfer

Source Server         : go-study
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : goimv1

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-10-13 21:46:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for usr
-- ----------------------------
DROP TABLE IF EXISTS `usr`;
CREATE TABLE `usr` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `mailbox` varchar(255) DEFAULT NULL COMMENT '邮箱-唯一',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `head_url` varchar(255) DEFAULT NULL COMMENT '头像url',
  `status` int(11) DEFAULT NULL COMMENT '个人状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `mailbox` (`mailbox`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';
