/*
Navicat MySQL Data Transfer

Source Server         : go-study
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : goimv1

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-10-13 21:46:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for relationship
-- ----------------------------
DROP TABLE IF EXISTS `relationship`;
CREATE TABLE `relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `mail_from` varchar(255) DEFAULT NULL COMMENT '发起人',
  `mail_to` varchar(255) DEFAULT NULL COMMENT '接收方',
  `setup_time` datetime DEFAULT NULL COMMENT '发起时间',
  `relation_grade` int(11) DEFAULT NULL COMMENT '关系等级',
  `secret` varchar(255) DEFAULT NULL COMMENT '联系密钥-唯一',
  PRIMARY KEY (`id`),
  UNIQUE KEY `from_to` (`mail_from`,`mail_to`),
  UNIQUE KEY `mail_from` (`mail_from`,`mail_to`),
  KEY `secret` (`secret`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COMMENT='好友关系表';
