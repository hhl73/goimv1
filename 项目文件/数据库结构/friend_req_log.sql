/*
Navicat MySQL Data Transfer

Source Server         : go-study
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : goimv1

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-10-13 21:46:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for friend_req_log
-- ----------------------------
DROP TABLE IF EXISTS `friend_req_log`;
CREATE TABLE `friend_req_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `mail_from` varchar(255) DEFAULT NULL COMMENT '好友请求发起方',
  `mail_to` varchar(255) DEFAULT NULL COMMENT '好友请求接收方',
  `send_time` datetime DEFAULT NULL COMMENT '好友请求发起时间',
  `req_body` varchar(255) DEFAULT NULL COMMENT '好友请求消息内容',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_time` (`mail_from`,`mail_to`,`send_time`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COMMENT='好友请求日志';
