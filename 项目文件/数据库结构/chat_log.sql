/*
Navicat MySQL Data Transfer

Source Server         : go-study
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : goimv1

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2021-10-13 21:46:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for chat_log
-- ----------------------------
DROP TABLE IF EXISTS `chat_log`;
CREATE TABLE `chat_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `mail_from` varchar(255) DEFAULT NULL COMMENT '消息发送人',
  `mail_to` varchar(255) DEFAULT NULL COMMENT '消息接收人',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `chat_body` varchar(255) DEFAULT NULL COMMENT '消息体',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='聊天消息日志';
