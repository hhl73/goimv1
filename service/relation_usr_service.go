// 好友关系服务：业务逻辑
package service

import (
	"encoding/json"
	"im-v1/config"
	"im-v1/database"
	"im-v1/model"
	"log"
)

// 根据 mailbox 查询用户
func FindUsrByMailbox(mailbox string) (*model.UsrOds, bool) {
	num, usr := model.FindByMailbox(mailbox)

	// 用户不存在
	if num == 0 {
		return nil, false
	}

	// 用户存在
	return usr.TransformOds(), true
}

// 根据 nickname 查询用户
func FindUsrByNickname(nickname string) ([]model.UsrOds, bool) {
	usrs := model.FindByNickname(nickname)

	// 用户不存在
	if len(usrs) == 0 {
		return nil, false
	}

	// 用户存在
	return usrs, true
}

// 建立好友关系
// 查找目标用户是否存在 || 添加自己 || 已经是好友关系了 (返回非法) ->
// 写入到 model.FriendReqLog -> 插入好友请求到redis
func AddRelation(from, to, body string) string {
	// 目标用户不存在 || 自己添加自己 || 已经是好友关系了
	f1, _ := model.NewRelationship(from, to, 1).IsAlreadyGoodFriend()
	f2, _ := model.NewRelationship(to, from, 1).IsAlreadyGoodFriend()
	if !IsAlreadyRegister(to) || from == to || (f1 && f2) {
		return "sorry, the target user was not found || already good friend"
	}

	// 写入到log
	friendLog := model.NewFriendReqLog(from, to, body)
	model.InsertFriendReqLog(friendLog)

	// 插入好友请求到redis -> to
	jsonLog, err := json.Marshal(friendLog)
	if err != nil {
		log.Println("[relation_usr_service AddRelation] ", err)
		return "log marshal -> failed send friendship request"
	}
	_, err = database.Redis.RPush(config.RelationMail+to, jsonLog).Result()
	if err != nil {
		log.Println("[relation_usr_service AddRelation] ", err)
		return "failed send friendship request"
	}

	// 正确返回
	return "friend request has been sent"
}

// 显示好友请求
// 不断 redis -> lpop 显示请求
func GetRelationRequest(mailbox string) []*model.FriendReqOds {
	// 获取长度
	length := database.Redis.LLen(config.RelationMail + mailbox).Val()

	// 无结果
	if length == 0 {
		return nil
	}

	// 每十个弹出一次
	reqs := make([]*model.FriendReqOds, 0)
	for i := int64(0); i < length && i < 10; i++ {
		// 取出数据
		reqBody, err := database.Redis.LPop(config.RelationMail + mailbox).Bytes()
		if err != nil {
			log.Println("[relation_usr_service GetRelationRequest] ", err)
			return reqs
		}
		// 反序列化
		req := &model.FriendReqOds{}
		err = json.Unmarshal(reqBody, req)
		if err != nil {
			log.Println("[relation_usr_service GetRelationRequest] ", err)
			return reqs
		}
		reqs = append(reqs, req)
	}
	return reqs
}

// 同意好友请求
// 需要确认Log中已经存在，防止非法操作 -> 插入两条relationship记录
func AcceptRelation(ods model.FriendReqOds, to string) string {
	// 非法操作
	if !ods.ExistFriendReq(to) {
		return "request invalid"
	}

	// 判断是否存在
	s1 := model.NewRelationship(ods.MailFrom, to, 1)
	s2 := model.NewRelationship(to, ods.MailFrom, 1)
	f1, _ := s1.IsAlreadyGoodFriend()
	f2, _ := s2.IsAlreadyGoodFriend()
	if f1 && f2 { // 二者都存在 说明 已经是好友了
		return "already good friends"
	}

	// 插入数据
	if !f1 {
		s1.InsertRelationship()
	}
	if !f2 {
		s2.InsertRelationship()
	}
	// 成功
	return "success"
}

// 显示好友关系 -> mailbox
func ShowFriendShip(mailbox string) []model.RelationshipOds {
	return model.FindGoodFriendshipOds(mailbox)
}

// 删除好友关系 -> 需要删除聊天 redis-key
func DelRelation(from, to string) string {

	// 删除好友
	ok := model.DeleteFriend(from, to)
	if !ok {
		return "Failed! friend delete failed"
	}

	// 删除聊天 redis-key
	p, q := compareAndSwap(from, to)
	database.Redis.Del(p + config.ChatConnector + q)

	// 返回
	return "friend delete successful"
}
