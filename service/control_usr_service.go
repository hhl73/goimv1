// 用户管理模块：业务逻辑
package service

import (
	"im-v1/config"
	"im-v1/model"
	"mime/multipart"
)

// 接收头像文件-存储
func SetHeadImg(mailbox string, file *multipart.FileHeader) (string, error) {

	// 新路径
	headURL := config.Tconf.Other.HeadStoreUrl + file.Filename

	// 上传到指定路径
	// 可以改成建立cos平台存储，而不是本地路径
	// 提供本地存储的模板，地址可以在配置文件（conf.yaml）修改
	/*
		err := tool.SaveMultipartFile(headURL, file)
		if err != nil {
			log.Printf("[control_usr_service SetHeadImg] %v", err)
			return "", err
		}
	*/

	// 更新数据库
	model.UpdateUsrWithHeadUrl(mailbox, headURL)

	return headURL, nil
}