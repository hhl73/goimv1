// 用于加密
package tool

import (
	"crypto/md5"
	"encoding/hex"
)

func EncryptMD5(key string) string {
	h := md5.New()
	h.Write([]byte(key))
	return hex.EncodeToString(h.Sum(nil))
}
