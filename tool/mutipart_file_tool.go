// multipart包下属file的处理工具
package tool

import (
	"errors"
	"io/ioutil"
	"log"
	"mime/multipart"
)

// 存储文件到目的地
// 入参： path、multipart.FileHeader
func SaveMultipartFile(path string, file *multipart.FileHeader) error {
	// 打开文件
	f, err := file.Open()
	if err != nil {
		log.Printf("[tool SaveMultipartFile] %v", err)
		return errors.New("open file failed")
	}
	defer f.Close()

	// 读取文件
	input := make([]byte, file.Size)
	_, err = f.Read(input)
	if err != nil {
		log.Printf("[tool SaveMultipartFile] %v", err)
		return errors.New("open file failed")
	}

	// 写入文件
	err = ioutil.WriteFile(path, input, 0666)
	if err != nil {
		log.Printf("[tool SaveMultipartFile] %v", err)
		return errors.New("save file failed")
	}

	return nil
}
