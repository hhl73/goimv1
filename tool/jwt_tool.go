// jwt身份验证工具
package tool

import (
	jwt "github.com/dgrijalva/jwt-go"
	"im-v1/config"
	"time"
)

var jwtSecret = []byte(config.Tconf.Other.JwtSecret)

type Claims struct {
	Id       int    `json:"id"`
	Mailbox  string `json:"mailbox"`
	Nickname string `json:"nickname"`
	jwt.StandardClaims
}

// 产生token的函数
func GenerateToken(id int, mailbox, nickname string, d time.Duration) (string, error) {

	// 设置过期时间
	nowTime := time.Now()
	expireTime := nowTime.Add(d)

	claims := Claims{
		id,
		mailbox,
		nickname,
		jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
		},
	}
	// 加密生成token
	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return tokenClaims.SignedString(jwtSecret)
}

// 验证token的函数
func ParseToken(token string) (*Claims, error) {

	// 解析token
	tokenClaims, err := jwt.ParseWithClaims(token, &Claims{},
		func(token *jwt.Token) (interface{}, error) {
			return jwtSecret, nil
		},
	)

	// 正确路径
	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}
	// token非法
	return nil, err
}
