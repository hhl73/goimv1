// 邮件验证码发送工具
// 来源我的另一个项目：https://gitee.com/g_night/ay-mail-model
package tool

import (
	"errors"
	"fmt"
	"gopkg.in/gomail.v2"
	"im-v1/config"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
)

// 发送者信息
//type SenderMsg struct {
//	Sender string `yaml:"Sender"` // 发送人
//	Alias  string `yaml:"Alias"`  // 发送人别名
//	Pass   string `yaml:"Pass"`   // 授权码
//	Host   string `yaml:"Host"`   // 所属服务器
//	Port   int    `yaml:"Port"`   // 发送端口
//}

/**
 * 发送邮件 - 纯文本
 * from：发送者， to：送达目标， subject：邮件主题， body：内容
 * err: 错误信息，无错误返回nil
 */
func SendMail(from *config.SenderMsg, to []string, subject, body string) error {
	// 1.检查初始化所有者信息
	if from == nil || from.Sender == "" || from.Pass == "" || from.Host == "" {
		log.Println("[SendMail] 发送者信息未初始化，可以调用 InitMailMsgByXXX 初始化")
		return errors.New("Sender information not initialized,maybe you should call  [InitMailMsgByXXX]")
	}

	// 2.填充个人信息
	d := gomail.NewDialer(from.Host, from.Port, from.Sender, from.Pass)

	// 3.设置邮件内容
	m := gomail.NewMessage()
	m.SetHeader("From", m.FormatAddress(from.Sender, from.Alias))
	m.SetHeader("To", to...)        //允许发送给多个用户
	m.SetHeader("Subject", subject) //设置邮件主题
	m.SetBody("text/html", body)    //设置邮件正文

	// 4.发送邮件
	return d.DialAndSend(m)
}

/**
 * 发送邮件 - 读入模板填充
 * from：发送者， to：送达目标， subject：邮件主题， modelPath：模板路径， replace：替换内容
 * err: 错误信息，无错误返回nil
 */
func SendMailWithModel(from *config.SenderMsg, to []string, subject, modelPath string, replace [][]string) error {
	// 1.读取模板文件
	model, err := ioutil.ReadFile(modelPath)
	if err != nil {
		fmt.Println("read fail", err)
	}

	// 2.替代模板信息，删除注释 <!--(.*?)-->
	body := string(model)
	reg := regexp.MustCompile(`<!--(.|[\r\n])*?-->`)
	body = reg.ReplaceAllString(body, "")
	for _, v := range replace {
		body = strings.ReplaceAll(body, v[0], v[1])
	}

	// 3.发送邮件
	return SendMail(from, to, subject, body)
}
