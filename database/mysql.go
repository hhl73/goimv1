package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"im-v1/config"
	"log"
)

var Db *gorm.DB

func InitMysql() {
	var err error
	baseConf := config.Tconf.Dbase
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		baseConf.User, baseConf.Password, baseConf.Host, baseConf.Port, baseConf.DBName)
	Db, err = gorm.Open("mysql", dsn)
	if err != nil {
		log.Fatalf("[mysql] connect error %v", err)
	}
	Db.DB().SetMaxOpenConns(10) // 设置最大连接数
	Db.DB().SetMaxIdleConns(5)  // 设置闲置情况最大连接数
}

func CloseMysql() {
	err := Db.Close()
	if err != nil {
		log.Println(err)
	}
}
