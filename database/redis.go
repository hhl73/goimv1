package database

import (
	"github.com/go-redis/redis"
	"im-v1/config"
	"log"
)

var (
	Redis *redis.Client
)

func InitRedis() {

	// 获取配置文件
	RedisConfig := config.Tconf.Redis

	Redis = redis.NewClient(&redis.Options{
		Addr:     RedisConfig.Addr,
		Password: RedisConfig.Password, // no password set
		DB:       RedisConfig.DB,       // use default DB
	})
	_, err := Redis.Ping().Result()
	if err != nil {
		log.Fatalf("[redis] 无法连接: %v", err)
	}
}

func CloseRedis() {
	err := Redis.Close()
	if err != nil {
		log.Fatalf("[redis] 关闭错误: %v", err)
	}
}
